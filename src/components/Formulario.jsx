import { useState } from 'react';
import PropTypes from 'prop-types';

import Alerta from './Alerta';

const Formulario = ({ actualizarInformacion }) => {


  const [ busqueda, actualizarBusqueda ] = useState({
    artista: '',
    cancion: ''
  });

  const [ error, actualizarError ] = useState(false);

  const { artista, cancion } = busqueda;

  const handleChange = e => {
    actualizarBusqueda({
      ...busqueda,
      [e.target.name]: e.target.value
    });
  }

  const handleSubmit = e => {
    e.preventDefault();
    if (artista.trim() === '' || cancion.trim() === '') {
      actualizarError(true);
      return;
    }
    actualizarError(false);

    actualizarInformacion(busqueda);
  }

  return (
    <div className="bg-info">
      <div className="container">
        <div className="row">
          <div className="col card text-white bg-transparent my-4 py-3">
            <fieldset>
              <legend className="text-center">
                Buscar letra y artista...
              </legend>

              <form
                onSubmit={ handleSubmit }
              >
                <div className="row">
                  {
                    error
                      ?
                    <div className="col-12 col-md-6 offset-md-3">
                      <Alerta
                        mensaje="Todos los campos son obligatorios"
                      />
                    </div>
                      :
                    null
                  }
                  <div className="col-md-6 ">
                    <div className="form-group">
                      <label htmlFor="artista">
                        Artista
                      </label>
                      <input
                        type="text"
                        name="artista"
                        className="form-control"
                        onChange={ handleChange }
                        value={ artista }
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="cancion">
                        Cancion
                      </label>
                      <input
                        type="text"
                        name="cancion"
                        className="form-control"
                        onChange={ handleChange }
                        value={ cancion }
                      />
                    </div>
                  </div>
                  <div className="col-12 col-md-3 offset-md-9">
                    <button
                      type="submit"
                      className="btn btn-primary btn-block"
                    >
                      Buscar
                    </button>
                  </div>
                </div>
              </form>
            </fieldset>
          </div>
        </div>
      </div>
    </div>
  );
};

Formulario.propTypes = {
  actualizarInformacion: PropTypes.func.isRequired
};

export default Formulario;