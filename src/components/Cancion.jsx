import { Fragment } from 'react';
import PropTypes from 'prop-types';

const Cancion = ({ letra }) => {

  if (letra.length === 0) return null;

  return (
    <Fragment>
      <h2>Letra</h2>
      <p>{ letra }</p>
    </Fragment>
  );
};

Cancion.propTypes = {
  letra: PropTypes.string.isRequired
};

export default Cancion;