import React from 'react';
import PropTypes from 'prop-types';

const Artista = ({ artista }) => {

  if(Object.keys(artista).length === 0) return null;

  const { strArtist, strArtistThumb, strGenre, strBiographyES, strBiographyEN, strFacebook, strTwitter, strLastFMChart } = artista;

  return (
    <div className="card border-light">
      <div className="card-header bg-primary text-light font-weight-bold">
        { strArtist }
      </div>
      <div className="card-body">
        <img src={ strArtistThumb } alt="Artista"/>
        <p className="card-text">
          <b>Genero: </b>{ strGenre }
        </p>
        <p className="card-text text-center">
          <a
            href={`https://${strFacebook}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="fab fa-facebook"></i>
          </a>
          <a 
            href={`https://${strTwitter}`} 
            target="_blank" 
            rel="noopener noreferrer"
          >
            <i className="fab fa-twitter"></i>
          </a>
          <a 
            href={`${strLastFMChart}`} 
            target="_blank" 
            rel="noopener noreferrer"
          >
            <i className="fab fa-lastfm"></i>
          </a>
        </p>

        <h2>Biografia</h2>
        <p className="card-text">
          {
            strBiographyES
              ?
            strBiographyES
              :
            strBiographyEN
          }
        </p>
      </div>
    </div>
  );
};

Artista.propTypes = {
  artista: PropTypes.object.isRequired
};

export default Artista;