import React from 'react';
import PropTypes from 'prop-types';

const Alerta = ({ mensaje }) => {
  return (
    <div className="alert alert-primary mt-3">
      <b>Ups!!! { mensaje }</b>
    </div>
  );
};

Alerta.propTypes = {
  mensaje: PropTypes.string.isRequired
};

export default Alerta;