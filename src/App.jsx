import { Fragment, useState, useEffect } from 'react';
import axios from 'axios';

import Formulario from './components/Formulario';
import Cancion from './components/Cancion';
import Artista from './components/Artista';

function App() {

  const [ informacion, actualizarInformacion ] = useState({});

  const [ letra, actualizarLetra ] = useState('');
  const [ artista, actualizarArtista ] = useState({});

  useEffect(() => {
    if (Object.keys(informacion).length === 0) return;

    consultar();
    // eslint-disable-next-line
  }, [informacion]);

  const consultar = async () => {
    const { artista, cancion } = informacion;
    const URILetra = `https://private-anon-8f1e6f0b0f-lyricsovh.apiary-mock.com/v1/${artista}/${cancion}`;
    const URIArtista = `https://www.theaudiodb.com/api/v1/json/1/search.php?s=${artista}`;

    actualizarLetra('');
    actualizarArtista({});

    const [ consultaLetra, consultaArtista ] = await Promise.all([
      axios.get(URILetra),
      axios.get(URIArtista)
    ]);
    
    actualizarLetra(consultaLetra.data.lyrics);
    actualizarArtista(consultaArtista.data.artists[0]);

  }


  return (
    <Fragment>
      <Formulario
        actualizarInformacion={ actualizarInformacion }
      />
      <div className="container mt-5">
        <div className="row">
          <div className="col-md-6">
            <Artista
              artista={ artista }
            />
          </div>
          <div className="col-md-6">
            <Cancion letra={ letra } />
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
